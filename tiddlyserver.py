#!/usr/bin/python3
"""
Script addapted from UBi's initial script found in TiddlyWiki's google group here:
https://groups.google.com/d/msg/tiddlywiki/M64suMWXDYQ/rmPOMiy-BAAJ
"""
import datetime, shutil, os
import ssl
import subprocess
import base64
from http.server import SimpleHTTPRequestHandler, HTTPServer
import hashlib



class ExtendedHandler(SimpleHTTPRequestHandler):
    args = None

    def do_OPTIONS(self):
        self.send_response(200, 'OK')
        self.send_header('allow','GET,HEAD,POST,OPTIONS,CONNECT,PUT,DAV,dav')
        self.send_header('x-api-access-type','file')
        self.send_header('dav','tw5/put')
        self.end_headers()
    
    def do_PUT(self):
        path = self.translate_path(self.path)

        if not  (self.args.GIT == "True"):
            self.makebackup(path)
            self.cleanupBackups(path)

        with open(path, "wb") as dst:
            dst.write(self.rfile.read(int(self.headers['Content-Length'])))

        if  (self.args.GIT == "True"):
            self.gitbackup()
        self.send_response(200, 'OK')
        self.end_headers()

    # Authentication stuff adapted from
    # https://github.com/tianhuil/SimpleHTTPAuthServer
    def do_HEAD(self):
        ''' head method '''
        self.send_response(200)
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def do_authhead(self):
        ''' do authentication '''
        self.send_response(401)
        self.send_header('WWW-Authenticate', 'Basic realm=\"Test\"')
        self.send_header('Content-type', 'text/html')
        self.end_headers()

    def check_password(self, hashed_password, user_password):
        '''
        adapted from 
        https://www.pythoncentral.io/hashing-strings-with-python/
        '''
        password, salt = hashed_password.split(':')
        return password == hashlib.sha256(salt.encode() + user_password).hexdigest()
        
    def do_GET(self):
        if "True" == self.args.DOAUTH:
            ''' Present frontpage with user authentication. '''
            gA = self.headers.get('Authorization')
            if str==type(gA):
                #it's "Basic "+passwd, so let's isolate passwd
                passwd = gA[6:]
                if self.check_password(self.args.KEY, base64.b64decode( passwd )):
                    SimpleHTTPRequestHandler.do_GET(self)
                else:
                    self.do_authhead()
                    self.wfile.write(gA.encode('utf-8'))
                    self.wfile.write('not authenticated'.encode('utf-8'))
            else:
                # elif gA is None:
                self.do_authhead()
                self.wfile.write('no auth header received'.encode('utf-8'))
        else:
            '''No user authentication'''
            SimpleHTTPRequestHandler.do_GET(self)
    
    def gitbackup(self):
        '''just adds all and commits, very minimal git usage
        this is called after every tiddly save
        '''
        tstamp=datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        try: 
            subprocess.run("git add --all", shell=True, check=True)
            subprocess.run("git commit -m '{}'".format(tstamp), shell=True, check=True)
            if int(self.args.VOCAL) >0:
                print("GIT updated")
        except:
            # printing in error so that it's visible even in the service logs
            print("GIT UPDATE ENCOUNTERED ERRORS:", file=sys.stderr)
            print(sys.exc_info()[0], file=sys.stderr)

    def cleanupBackups(self,src):
        keep_last_n = int(self.args.SAVELASTX)
        srcpath, srcfile = os.path.split(src)
        srcname, src_ext = os.path.splitext(srcfile)
        dstpath = os.path.join(srcpath,'./twBackups')

        # datetime string to datetime object
        dtsTodto = lambda s: datetime.datetime.strptime(s, "%Y%m%d%H%M%S")
        # filename to datetime object
        fnTodto = lambda fn:  dtsTodto(fn.split('-')[1][:-5])
        # get the backuped files that correspond to srcname
        bkupfile_list = [x for x in os.listdir(dstpath) if (x.split('-')[0]==srcname)]
        # this is a list of the filenames plus each filename's datetime object
        # (to allow sorting)

        sorted_list = sorted([(x,fnTodto(x)) for x in bkupfile_list], key=lambda y:y[1])

        # print("We have this many files: ",len(sorted_list))
        list_count=len(sorted_list)
        # print("To keep the last X, I need to delete the first", len(sorted_list)-keep_last_n)
        delete_first = max([ (len(sorted_list)-keep_last_n),0])

        if int(self.args.VOCAL) >0:
            print("Deleting the following backup files")
            for t in sorted_list[:delete_first]:
              print(t)
        # print("I keep these:")
        # for t in sorted_list[-keep_last_n:]:
        #   print(t)

        #delete them:
        for thing in sorted_list[:delete_first]:
            fp = os.path.join( dstpath,thing[0] )
            if os.path.exists(fp):
                os.remove(fp)

    def makebackup(self,src):
        (srcpath, srcfile) = os.path.split(src)
        (srcname, src_ext) = os.path.splitext(srcfile)

        tstamp=datetime.datetime.now().strftime("%Y%m%d%H%M%S")
        dstpath = os.path.join(srcpath,'./twBackups')
        if not os.path.exists(dstpath):
            os.mkdir(dstpath)
        final_dest = os.path.join(dstpath, srcname+'-'+tstamp+src_ext)
        shutil.copyfile(src, final_dest)
        if int(self.args.VOCAL) >0:
            print("Saved wiki in {}".format(final_dest))




class objectview(object):
    '''
    makes dictionary keys available as object attributes
    instead of having to say d[key] you can do d.key
    '''
    def __init__(self, d):
        self.__dict__ = d

def read_conf(path):
    options={}
    with open(path,'r') as fi:
        for line in fi:
            if (len(line)>1) and (not line.startswith("#")):
                a,b = line.split()
                options[a] = b
    return objectview(options)

def main():
    ipath=os.path.dirname(os.path.abspath(__file__))
    os.chdir(ipath+"/served" )
    args = read_conf(ipath+"/tiddlyserver.config")


    ExtendedHandler.args = args
    theserver = HTTPServer((args.IP, int(args.PORT)),ExtendedHandler)

    if "True" == args.HTTPS:
        assert os.path.isfile(args.KEY_PATH), "ssl key not found, make sure you have permission to read the file"
        assert os.path.isfile(args.CERT_PATH), "ssl certificate not found, make sure you have permission to read the file"
        theserver.socket = ssl.wrap_socket (theserver.socket, 
                keyfile=args.KEY_PATH, 
                certfile=args.CERT_PATH, server_side=True)
    
    if int(args.VOCAL) >0:
        print("Starting server on ip: {} , port: {}".format(args.IP, args.PORT))
        if "True" == args.DOAUTH :
            print("Username and password will be needed to access your wiki")

        if "True" == args.HTTPS :
            print("HTTPS is set to True, the paths of the keys are the following")
            print("key: {}".format(args.KEY_PATH))
            print("certificate: {}".format(args.CERT_PATH))

        if "True" == args.GIT :
            print("GIT saving is enabled, this won't work in Windows")
        else:
            print("Legacy backup is enabled, the last {} saves of your wiki are stored in the ./twBackups folder".format(args.SAVELASTX))

    theserver.serve_forever()

if __name__ == '__main__':
    main()
