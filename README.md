This was addapted from UBi's initial script found in TiddlyWiki's google group here:    
https://groups.google.com/d/msg/tiddlywiki/M64suMWXDYQ/rmPOMiy-BAAJ       

This is a more convoluted version of the idea, with the intention of serving the wiki over the internet. If you just want to work with a wiki on your own machine, then UBi's version is simpler and *just works*. This repo includes things like https, user authentication and git version control.      
     
Contibutions are very welcome


# You will need      
* Python3    
* git (optional)     
      
# How to use this      
## On a Linux system, serving the wiki over the internet (or local network)
* Clone the repository in your home folder (that's where I keep mine at least). 
* Run the tiddlypass script (with python3) to make a hashed key of your username and password        
* Paste that in the tiddlyserver.config file, along with your other specific details such as ip and port, and https keys       
* You can then run the server with     
```bash
python3 ./tiddlyserver.py
```
* Alternatively, you can make this into a linux service for systemd (read below), this will always keep the server up.      

## Serving the wiki on your own machine /  Disabling features / Windows users
If you intend to just serve a wiki on your own machine: 
* You won't need https or user authentication. To disable those features, set the following options in the tiddlyserver.config to "False"
	* DOAUTH
	* HTTPS
* If you are a windows user, the git backup scheme will not work (untill someone helps me on how to call git with a subprocess in python)
	* Set the GIT option to "False"

## Default wiki
As it stands, the server will serve a directory view of the served folder which can work as a list of your avilable wikis.       
If you prefer to have a default wiki you can either manually go to a wiki (e.g. https://mysite.com:6969/defaultwiki.html ), or have one wiki
named index.html. This would cause the server to default to that wiki (i.e. https://mysite.com:6969 would land you on your index.html wiki ) but you would not be able to see the directory view and would have to manually maintain links to your other wikis.

# How to make it into a service (Linux systems with systemd)
* Edit the .service file to fix the paths    
* Make a symbolic link in /etc/systemd/ (you'll need root access for this)\
```bash
ln -s /home/user/tiddlyserver/tiddly.service /etc/systemd/system/
```
* Restart the systemctl daemon\
```bash
sudo systemctl daemon-reload
```    
* Start the service
```bash
sudo service tiddly start
# you can then try
sudo service tiddly status
# you can then try
sudo service tiddly stop
```

# Possible caveats    

This is a git in a git. I'm not sure how sane that is, but it seems to work and this is just a weekend project.     
It might be a good idea to remove the git from the outer folder after cloning the repository.     
      